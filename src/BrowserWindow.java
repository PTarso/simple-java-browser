import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;



public class BrowserWindow extends JFrame{
	
	private JTextField addressBar;
	private JEditorPane display;
	private JButton goBtn;
	private ImageIcon logo;
	
	// CONSTRUCTOR
	public ReadFile() {
		// Title
		super("Simple Browser");
		
		logo = new ImageIcon("./res/ikagyaa1.png");
		super.setIconImage(logo.getImage());
		addressBar = new JTextField("Enter your URL here");
		goBtn = new JButton("Go");
		addressBar.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent event) {
						loadStuff(event.getActionCommand());
					}
				}
		);
		add(addressBar, BorderLayout.NORTH);
		
		display = new JEditorPane();
		display.setEditable(false);
		display.addHyperlinkListener(
				
				new HyperlinkListener(){
					
					public void hyperlinkUpdate(HyperlinkEvent event) {
						if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
							loadStuff(event.getURL().toString());
							
						}						
					}
				}
		
		);
		add(new JScrollPane(display), BorderLayout.CENTER);
		setSize(500, 300);
		setVisible(true);
	}
	
	// Load things to display on the screen
	// This is very old code from my first java days. I definitely would NOT do this if I was coding this today
	private void changeAndLoadPage(String userText) {
		try {
			display.setPage(userText);
			addressBar.setText(userText);
		} catch(Exception e) {
			try {
				display.setPage("http://" + userText);
			} catch (Exception err) {
				System.out.println(String.format("Error: %s", err.getMessage()));
			}
		}
	}
	
}
